__author__ = 'Kalmor'

# Imports
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import settings
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash

# create application and initialise
app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def hello_world():
     return 'Hello World'

# database connection
def connect_db():

     return create_engine(URL(**settings.DATABASE))

# run file standalone
if __name__ == '__main__':
    app.run()
