-- Database: "FurStats"

-- DROP DATABASE "FurStats";

CREATE DATABASE "FurStats"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

-- Table: users

-- DROP TABLE users;

CREATE TABLE users
(
  userid serial NOT NULL,
  username text NOT NULL,
  password text NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (userid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;